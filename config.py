import os

GEO_PICKER = [
    ["Argentina", "AR"],
    ["Australia", "AU"],
    ["Austria", "AT"],
    ["Belgium", "BE"],
    ["Brazil", "BR"],
    ["Canada", "CA"],
    ["Chile", "CL"],
    ["Colombia", "CO"],
    ["Czechia", "CZ"],
    ["Denmark", "DK"],
    ["Egypt", "EG"],
    ["Finland", "FI"],
    ["France", "FR"],
    ["Germany", "DE"],
    ["Greece", "GR"],
    ["Hong Kong", "HK"],
    ["Hungary", "HU"],
    ["India", "IN"],
    ["Indonesia", "ID"],
    ["Ireland", "IE"],
    ["Israel", "IL"],
    ["Italy", "IT"],
    ["Japan", "JP"],
    ["Kenya", "KE"],
    ["Malaysia", "MY"],
    ["Mexico", "MX"],
    ["Netherlands", "NL"],
    ["New Zealand", "NZ"],
    ["Nigeria", "NG"],
    ["Norway", "NO"],
    ["Peru", "PE"],
    ["Philippines", "PH"],
    ["Poland", "PL"],
    ["Portugal", "PT"],
    ["Romania", "RO"],
    ["Russia", "RU"],
    ["Saudi Arabia", "SA"],
    ["Singapore", "SG"],
    ["South Africa", "ZA"],
    ["South Korea", "KR"],
    ["Spain", "ES"],
    ["Sweden", "SE"],
    ["Switzerland", "CH"],
    ["Taiwan", "TW"],
    ["Thailand", "TH"],
    ["Türkiye", "TR"],
    ["Ukraine", "UA"],
    ["United Kingdom", "GB"],
    ["United States", "US"],
    ["Vietnam", "VN"]
]

TIME_PICKER = [
    ["Past 12 months", 0],
    ["Past hour", "?date=now%201-H"],
    ["Past 4 hours", "?date=now%204-H"],
    ["Past day", "?date=now%204-H"],
    ["Past 7 days", "?date=now%207-d"],
    ["Past 30 days", "?date=today%201-m"],
    ["Past 90 days", "date=today%203-m"],
    ["Past 5 years", "?date=today%205-y"],
    ["2004 - present", "?date=today%205-y"]
]


GOOGLE_TRENDS_LINK = "https://trends.google.com/trends/explore"
GOOGLE_TRENDS_CONNECTION_ERROR = 'Error 429 (Too Many Requests)!!1'
MAX_AMOUNT_OF_SEARCH_SUGGESTIONS = 4
SLEEP_TIME_FOR_UPLOADING_DATA = 3
CURRENT_DIR = os.getcwd()

