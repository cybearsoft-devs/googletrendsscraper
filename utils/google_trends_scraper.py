import time

from selenium.webdriver.common.by import By
from selenium.webdriver import Keys
from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager

from utils.utils import select_option
from config import (
    GOOGLE_TRENDS_LINK,
    TIME_PICKER,
    GEO_PICKER,
    SLEEP_TIME_FOR_UPLOADING_DATA,
    GOOGLE_TRENDS_CONNECTION_ERROR,
    MAX_AMOUNT_OF_SEARCH_SUGGESTIONS
    )


class GoogleTrendsScraper:
    """
    A class for automating the scraping of data from Google Trends.

    This scraper automates a web browser to interact with Google Trends, allowing users
    to select search criteria such as time frame, country, and specific search types.
    It then downloads the results to a specified folder.

    Attributes:
        download_folder (str): The path where downloaded files will be saved.
        user_search_input (str): User input for the search query.
        driver: The Selenium WebDriver used for automating browser interactions.
    """

    def __init__(self, user_search_input, download_folder, time_frame_index, country_index, headless=True):
        """
        
            
        """
        self.user_search_input = user_search_input
        self.download_folder = download_folder
        self.time_frame = time_frame_index
        self.country = country_index
        self.driver = self._init_driver(headless)

    def scrape(self):
        """
        Executes the scraping process on Google Trends.

        This method goes through the process of selecting the time frame, opening Google Trends page,
        selecting the search box, applying user-selected search options, selecting the country,
        saving the results, and finally quitting the driver.
        """
        date_query_param = self._select_time_frame()
        self._open_google_trends_page(date_query_param)
        search_box = self._select_search_box()
        user_selected_option_number = self._select_search_type(search_box)
        self._apply_user_selection(user_selected_option_number, search_box)
        self._select_country()
        self._save_results()
        self.driver.quit()
    
    def _init_driver(self, headless):
        """
        Initializes the scraper with user input and configuration settings.

        This constructor sets up the necessary attributes for the scraping process based
        on the user's input and selected options. It also initializes the web driver used
        for automation.

        Args:
            user_search_input (str): The search query input by the user.
            download_folder (str): The path to the folder where the scraper will save downloaded files.
            time_frame_index (int): The index representing the user's choice of time frame.
            country_index (int): The index representing the user's choice of country.
            headless (bool, optional): Flag to determine if the browser should run in headless mode.
                                    Defaults to True.
        """
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_experimental_option("prefs", {
            "download.default_directory": self.download_folder,
            "download.prompt_for_download": False,
            "download.directory_upgrade": True,
            "safebrowsing.enabled": True,
            'useAutomationExtension': False
        })
        chrome_options.add_argument("--ignore-certificate-errors")
        chrome_options.add_argument("user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36")
        chrome_options.headless = headless
        driver = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()), options=chrome_options)
        return driver

    def _select_time_frame(self):
        """
        Allows the user to select a time frame for Google Trends data.

        The method displays available time frame options, gets user input, and navigates
        the WebDriver to the appropriate URL based on the selection.

        Returns:
            str: The query parameter for the selected time frame.
        """
        if self.time_frame != 1:
            self.driver.get(GOOGLE_TRENDS_LINK + TIME_PICKER[self.time_frame - 1][1])
        return TIME_PICKER[self.time_frame - 1][1] or ""

    def _select_country(self):
        """
        Selects a country on the Google Trends page based on user input.
        """
        country_name = GEO_PICKER[self.country - 1][0]
        custom_select = self.driver.find_element(By.CLASS_NAME, "hierarchy-select")
        custom_select.click()
        choose_country = self.driver.find_element(By.ID, value='input-10')

        choose_country.send_keys(country_name)
        choose_country.send_keys(Keys.ARROW_DOWN)
        choose_country.send_keys(Keys.RETURN)

    def _save_results(self):
        """
        Attempts to save the results from the Google Trends page.

        The method looks for export buttons on the page and clicks them to download the data.
        It also handles cookie consent prompts if present.
        """
        time.sleep(SLEEP_TIME_FOR_UPLOADING_DATA)
        buttons = self.driver.find_elements(By.CSS_SELECTOR, "button.widget-actions-item.export")
        if len(buttons) < 1:
            print("Could not export results")
            return
        
        ok_button = self.driver.find_element(By.CLASS_NAME, "cookieBarButton.cookieBarConsentButton")
        ok_button.click()
        time.sleep(SLEEP_TIME_FOR_UPLOADING_DATA * 4)
        for button in buttons:
            button.click()
            time.sleep(1)
        print(f"All results has been saved to: {self.download_folder}")

    def _open_google_trends_page(self, query_param):
        """
        Opens a specific Google Trends page based on the provided query parameter.

        Args:
            query_param (str): The query parameter to append to the Google Trends URL.
        """
        self.driver.get(GOOGLE_TRENDS_LINK + query_param)
        if self.driver.title == GOOGLE_TRENDS_CONNECTION_ERROR:
            self._open_google_trends_page(query_param)

    def _select_search_type(self, search_box):
        """
        Allows the user to select a search type based on the provided suggestions.

            This method fetches search suggestions based on the user's input and displays a list 
            of options to the user. The user is then prompted to choose one of these options.

            Args:
                search_box: The search box WebElement on the Google Trends page.

            Returns:
                int: The number representing the user's chosen option from the list of suggestions.
        """
        suggestions = self._get_search_suggestions(search_box, self.user_search_input)
        options = suggestions[:MAX_AMOUNT_OF_SEARCH_SUGGESTIONS]
        choice = select_option(options)
        return choice

    def _apply_user_selection(self, user_selected_option_number, search_box):
        """
        Applies the user's selected search option to the Google Trends search.

        This method takes the user's selected option number, navigates through the suggestion 
        list in the search box using keyboard inputs, and initiates the search.

        Args:
            user_selected_option_number (int): The number representing the user's selection in the suggestions list.
            search_box: The search box WebElement on the Google Trends page.

        Returns:
            None
        """
        search_box.clear()
        search_box.send_keys(self.user_search_input)
        time.sleep(SLEEP_TIME_FOR_UPLOADING_DATA)
        for _ in range(user_selected_option_number):
            search_box.send_keys(Keys.ARROW_DOWN)
        search_box.send_keys(Keys.RETURN)

    def _get_search_suggestions(self, search_box, user_search_input):
        """
        Retrieves search suggestions based on the user's input.

        Args:
            search_box: The search box WebElement on the Google Trends page.
            user_search_input (str): The user's search input.

        Returns:
            list: A list of suggestion strings.
        """
        search_box.send_keys(user_search_input)
        time.sleep(SLEEP_TIME_FOR_UPLOADING_DATA)
        suggestions = []
        titles = self.driver.find_elements(By.CSS_SELECTOR, ".autocomplete-entity.autocomplete-title")
        descriptions = self.driver.find_elements(By.CSS_SELECTOR, ".autocomplete-entity.autocomplete-desc")

        if len(titles) == len(descriptions):
            for title, desc in zip(titles, descriptions):
                title_text = title.text
                desc_text = desc.text
                suggestions.append([f"{title_text} - {desc_text}", None])
        return suggestions   
    
    def _select_search_box(self):
        """
        Selects and returns the search box WebElement on the Google Trends page.

        Returns:
            WebElement: The search box element.
        """
        time.sleep(SLEEP_TIME_FOR_UPLOADING_DATA)
        search_box = self.driver.find_element(By.ID, "input-29")
        return search_box
