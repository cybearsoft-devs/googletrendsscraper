import os
from datetime import datetime

from config import (
     CURRENT_DIR
)


def get_download_folder(user_search_input):
    """
    Prompts the user for search keywords and generates a download folder path.

    The method asks the user to input key words for the search. It then
    generates a relative path for the download folder, using the current
    timestamp to ensure uniqueness. The full path is constructed using
    the current directory and the generated relative path.

    Returns:
        tuple: A tuple containing the full path to the download folder and the user's search input.
                Format: (download_folder, user_search_input)
    """
    
    new_folder_relative_path = f"output/{user_search_input}_{int(datetime.timestamp(datetime.utcnow()))}"
    download_folder = os.path.join(CURRENT_DIR, new_folder_relative_path)
    return download_folder

def select_option(options):
        """
        Prompts the user to select an option from a list of available options.

        Returns:
            int: The user's choice of option as an integer.
        """
        print("Choose one of the options for time frame selection:")
        for index, option in enumerate(options):
            print(f"{index + 1} ||| {option[0]}")
        choice = input("Enter the number of your choice: ")
        try:
            choice = int(choice)
            if 1 <= choice <= len(options):
                selected_option = options[choice - 1]
                print(f"You chose: {selected_option}")
                return choice
            else:
                print("Invalid choice. Please enter a valid number.")
                select_option(options)
        except ValueError:
            print("Invalid input. Please enter a number.")
            select_option(options)
