from utils.google_trends_scraper import GoogleTrendsScraper
from config import (
    GEO_PICKER,
    TIME_PICKER
)
from utils.utils import (
    select_option,
    get_download_folder
)

def main_loop():
    """
    Runs the main loop for scraping Google Trends data.

    This function continuously executes the scraping process using the GoogleTrendsScraper class.
    After each scraping session, it prompts the user to decide whether to continue with another
    search. The loop continues until the user decides to stop.
    """
    while True:
        user_search_input = input("Please enter key words for search \n")
        folder = get_download_folder(user_search_input)
        time_frame_index = select_option(TIME_PICKER)
        country_index = select_option(GEO_PICKER)
        GoogleTrendsScraper(user_search_input, folder, time_frame_index, country_index).scrape()
        should_continue = input("Do you want to continue the search? (Y/N): ")
        if should_continue.lower() != 'y':
            break

if __name__ == "__main__":
    main_loop()